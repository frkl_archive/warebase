# -*- coding: utf-8 -*-
from flask import Blueprint, request, Response

from warebase.plugins.codereadr.models import ScannedItem
from warebase.app.database import database as db

codereadr = Blueprint("codereadr", __name__, template_folder="templates")


# flake8: noqa


@codereadr.route("/")
def index():

    print("hello!!!!")

    item = ScannedItem(name="lego")

    db.session.add(item)
    db.session.commit()

    return "status: ok", 200


@codereadr.route("/scan_item", methods=["POST"])
def register_scan():

    data = request.form

    import pp

    pp(data)

    tid = data["tid"]
    sid = data["sid"]
    udid = data["udid"]
    userid = data["userid"]
    questions = data.get("questions", None)
    answers = data["answers"]
    capture_type = data.get("capture_type", None)
    time_zone = data.get("time_zone", None)
    gps_location = data.get("gps_location", None)

    xml = """<?xml version="1.0" encoding="UTF-8"?>
<xml>
    <message>
        <status>1</status>
        <text>Thank you for scanning with codeREADr</text>
    </message>
</xml>"""

    return Response(xml, mimetype="text/xml")
