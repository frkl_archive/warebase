# -*- coding: utf-8 -*-
from warebase.app.core import create_app
from warebase.models.article import Article  # noqa
import warebase.plugins.codereadr.models  # noqa

app = create_app()


# @app.shell_context_processor
# def make_shell_context():
#     from warebase.models.article import Article
#     return dict(app=app, db=database, Article=Article)
