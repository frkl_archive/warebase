[![PyPI status](https://img.shields.io/pypi/status/warebase.svg)](https://pypi.python.org/pypi/warebase/)
[![PyPI version](https://img.shields.io/pypi/v/warebase.svg)](https://pypi.python.org/pypi/warebase/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/warebase.svg)](https://pypi.python.org/pypi/warebase/)
[![Pipeline status](https://gitlab.com/frkl/warebase/badges/develop/pipeline.svg)](https://gitlab.com/frkl/warebase/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# warebase

*Central department store database*


## Description

Documentation still to be done.

# Development

## Create development environment

### Using *freckles*

Make sure you install/update freckles before use:

    curl https://freckles.sh | UPDATE=true bash
    source $HOME/.profile         # (if necessary)


Then we need to unlock the *freckles* [context](https://freckles.io/doc/configuration/contexts) (so we can use 'remote' frecklets),
and execute the [warebase-dev-environment](https://gitlab.com/frkl/warebase/blob/develop/.freckles/warebase-dev-environment.frecklet) frecklet:

    freckles context unlock       # (if necessary -- agree to terms)
    frecklecute -c allow_remote=true gl:frkl/warebase/.freckles/warebase-dev-environment.frecklet $(pwd)

After that, we should have [pyenv](https://github.com/pyenv/pyenv) installed, including a reasonably recent version of Python,
a virtualenv with the name 'warebase-dev' (at ``$HOME/.pyenv/versions/warebase-dev``) that holds our project incl. all dependencies we need to do development, and a
``.python-version`` file that activates the 'warebase-dev' virtualenv whenever we ``cd`` into the project dir.  

### Manually

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'warebase' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 warebase
    git clone https://gitlab.com/frkl/warebase
    cd <warebase_dir>
    pyenv local warebase
    pip install -e .[develop,testing,docs]
    pre-commit install

## Vagrant

It's neat to be able to run the development code directly on a Virtual Machine that has PostgreSQL and RabbitMQ (for celery)
installed. Here's how to set that up.

### Create Vagrant box incl. Database & Celery services

For this, we create a Vagrant box with its own IP address, so we don't need to worry about port forwarding and such. Figure out the name of the
network adapter you want to use (``ip a`` -- result would be something like ``enp0s25`` or ``wlp3s0``). Also, pick a (free) IP address in the network that
adapter is connected to.

    cd  <warebase_dir>
    frecklecute --community warebase-vagrant --vagrant-bridge <network_device> --vagrant-ip <static_ip_for_vagrant_box> --warebase-path $(pwd)

After this, you should be able to visit the webapp with a browser on a url like: http://YOUR_VAGRANT_IP/codereadr .  

### Restart 'warebase' & 'celery' services in Vagrant box

The *flask* app we are running does not pick up changes to the source code automatically, so we need to restart our services
every time we make changes and want to see the result:

    frecklecute vagrant-warebase-restart

### Create new Database migration

Whenever we make changes to our data models, we need to create a database migration script:

    frecklecute vagrant-warebase-migrate

### Upgrade database schema (when there are new migrations)

Whenever there was a change to the data model, after we create a new migration script, we need up upgrade our database:

    frecklecute vagrant-warebase-upgrade  

## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!).

[The Prosperity Public License v2.0.0](https://licensezero.com/licenses/prosperity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
