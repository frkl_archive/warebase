"""empty message

Revision ID: 4b95497a27a4
Revises: 415ccb413456
Create Date: 2019-07-05 19:26:00.289283

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.ddl import CreateColumn

revision = '4b95497a27a4'
down_revision = '415ccb413456'
branch_labels = None
depends_on = None


def upgrade(engine_name):
    globals()["upgrade_%s" % engine_name]()


def downgrade(engine_name):
    globals()["downgrade_%s" % engine_name]()


def upgrade_():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('codereadr_scanned_items',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Text(), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_codereadr_scanned_items'))
    )
    # ### end Alembic commands ###


def downgrade_():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('codereadr_scanned_items')
    # ### end Alembic commands ###

