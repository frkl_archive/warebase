# -*- coding: utf-8 -*-
from sqlalchemy.dialects.postgresql import JSONB

from warebase.app.database import database as db


class Article(db.Model):

    __tablename__ = "articles"
    # field_seq = Sequence('article_id_seq')
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    misc = db.Column(JSONB)

    def __repr__(self):
        return "<User %r>" % self.username
