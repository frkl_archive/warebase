# -*- coding: utf-8 -*-
import logging
import os

from flask import Flask

from warebase.app.database import database
from warebase.app.migrate import migrate
from warebase.app.tasks import celery
from warebase.plugins.codereadr.codereadr import codereadr

logger = logging.getLogger()

CONFIG_KEYS = ["CELERY_BROKER_URL", "SQLALCHEMY_DATABASE_URI"]


def create_app(debug=False):
    return entrypoint(debug=debug, mode="app")


def create_celery(debug=False):
    return entrypoint(debug=debug, mode="celery")


def entrypoint(debug=False, mode="app"):

    assert isinstance(mode, str), 'bad mode type "{}"'.format(type(mode))
    assert mode in ("app", "celery"), 'bad mode "{}"'.format(mode)

    app = Flask(__name__)

    app.debug = debug

    configure_app(app)

    configure_logging(debug=debug)

    database.init_app(app)
    migrations_dir = os.path.join(
        os.path.dirname(__file__), "..", "external", "migrations"
    )
    migrate.init_app(app, database, directory=migrations_dir)

    configure_celery(app, celery)

    # register blueprints
    app.register_blueprint(codereadr, url_prefix="/codereadr")

    if mode == "app":
        return app
    elif mode == "celery":
        return celery


def configure_app(app):

    logger.info("configuring flask app")

    sql_alchemy_binds = {}

    for key in CONFIG_KEYS:

        value = os.environ.get(key)
        if value is None:
            raise Exception("Missing config value: {}".format(key))
        logger.debug("Adding config key: {}".format(key))
        app.config[key] = os.environ.get(key)

    app.config["SQLALCHEMY_BINDS"] = sql_alchemy_binds
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


def configure_celery(app, celery):

    # set broker url and result backend from app config
    # celery.conf.broker_url = app.config["CELERY_BROKER_URL"]

    celery.conf.update(app.config)
    # celery.conf.result_backend = app.config['CELERY_RESULT_BACKEND']

    # subclass task base for app context
    # http://flask.pocoo.org/docs/0.12/patterns/celery/
    TaskBase = celery.Task

    class AppContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = AppContextTask

    # run finalize to process decorated tasks
    celery.finalize()


def configure_logging(debug=False):

    root = logging.getLogger()
    h = logging.StreamHandler()
    fmt = logging.Formatter(
        fmt="%(asctime)s %(levelname)s (%(name)s) %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S",
    )
    h.setFormatter(fmt)

    root.addHandler(h)

    if debug:
        root.setLevel(logging.DEBUG)
    else:
        root.setLevel(logging.INFO)
