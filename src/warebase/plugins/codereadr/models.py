# -*- coding: utf-8 -*-
from warebase.app.database import database as db


class ScannedItem(db.Model):

    __tablename__ = "codereadr_scanned_items"
    # field_seq = Sequence('article_id_seq')
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return "<User %r>" % self.username
